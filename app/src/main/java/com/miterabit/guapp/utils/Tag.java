package com.miterabit.guapp.utils;

/**
 * Created by cifue on 15/03/2018.
 */

public class Tag {
    public static final String LECTURA = "lectura";
    public static final String EXTRA_NOMBRE = "0x0001";
    public static final String EXTRA_CUI = "0x0002";
    public static final String EXTRA_CARNET = "0x0003";
    public static final String TAG_MAIN = "main_activity";
    public static final String TAG_AUTH_REGISTER = "main_registro";
    public static final String CLOUD_STORAGE = "cloud_storage";
    public static final String DENUNCIA = "denuncia";
    public static final String KEY_DETALLE_FINAL = "timeline";
    public static final String PREFERENCIAS_GUARDADAS = "preferencias_sistema";
    public static final String ADAPTADOR = "recycler_adapter";
    public static final String DETALLE = "detalle_activity";
    public static final String TOKEN = "token_rfg";
    public static final String VERIFICACION = "verificar_baneo";
    public static final String ADMIN_LVL = "admin_level";
    public static final String WEBVIEW = "webview_url";
    public static final String EXTRA_NN = "nn";
    public static final String COMENTAR = "comentar_estado";
    public static final String ADMIN_AUTH = "admin_tag";
}
