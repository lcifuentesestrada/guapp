package com.miterabit.guapp.utils;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;
import com.miterabit.guapp.R;

import java.io.File;

public class ActivityPdf extends AppCompatActivity {

    PDFView pdfView;
    File pdfReference = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);

            displayFromUrl();

    }

    private void displayFromUrl( ) {

        pdfView = (PDFView) findViewById(R.id.pdfView);
        pdfReference = new File(Environment.getExternalStorageDirectory() + "/" + "gu" + "/" + "gu" + ".pdf");
        pdfView.fromFile(pdfReference).load();

    }
}
