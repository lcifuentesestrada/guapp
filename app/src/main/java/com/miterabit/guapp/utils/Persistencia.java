package com.miterabit.guapp.utils;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by cifue on 23/03/2018.
 */

public class Persistencia extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
