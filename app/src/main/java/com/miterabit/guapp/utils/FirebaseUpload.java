package com.miterabit.guapp.utils;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.miterabit.guapp.modelo.Denuncias;

import java.util.Calendar;
import java.util.Date;

public class FirebaseUpload extends Service {


    StorageReference imageReference;
    StorageReference fileRef;
    public static int progreso = 0;


    public FirebaseUpload() {
        super();
    }

    @Override
    public void onCreate() {
        Log.i(Tag.CLOUD_STORAGE, "servicio creado");
        super.onCreate();
        progreso = 0;
    } //cuando se crea el servicio


    public static int getProgreso() {
        return progreso;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        progreso = 0;

        Log.i(Tag.CLOUD_STORAGE, "servicio inciado...");

        String codigoContador = null; //iniciar código del contador

        if (intent != null) {
            codigoContador = intent.getStringExtra("correo"); //obtener extra de codigo contador
            Uri fileUri = Uri.parse(intent.getStringExtra("data")); // obtener extra de la uri donde se encuentra el archivo
            final String titulo = intent.getStringExtra("titulo");
            final String descripcion = intent.getStringExtra("descripcion");
            final String extencion = intent.getStringExtra("extencion");
            final String usuario = intent.getStringExtra("usuario");


            Calendar calendar = Calendar.getInstance();
            final int num_mes = calendar.get(Calendar.MONTH) + 1;// Aquí obtengo el dia del mes android tiene enero como 1 entonces se suma +1
            final int num_year = calendar.get(Calendar.YEAR);// Aquí obtengo el dia del año


            //referencia a la carpeta en storage donde sera insertado.
            imageReference = FirebaseStorage.getInstance().getReference().child("pruebas").child(codigoContador).child(num_mes + "-" + num_year);

            final long millis = new Date().getTime();

            fileRef = imageReference.child(String.valueOf(millis)); // nombre del archivo en cloud storage
            fileRef.putFile(fileUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            String name = taskSnapshot.getMetadata().getName();  //obtener nombre del archivo que se subio.
                            String url = taskSnapshot.getDownloadUrl().toString(); //obtener la url del archivo que se subio.

                            Log.i(Tag.CLOUD_STORAGE, "Uri: " + url);
                            Log.i(Tag.CLOUD_STORAGE, "Name: " + name);

                            if (url != null) { //cuando la url sea diferente ha null hacer la inserción
                                writeNewUser(usuario, titulo, descripcion, url, extencion);


                            }


                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Log.e(Tag.CLOUD_STORAGE, "error: " + exception);
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            Log.i(Tag.CLOUD_STORAGE, "subiendo: " + progress + "% "); //progreso de subida

                            progreso = (int) progress;
                            Log.d(Tag.CLOUD_STORAGE, ": " + progreso);


                        }
                    })
                    .addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                            Log.i(Tag.CLOUD_STORAGE, "subida pausada");
                        }
                    });
        }

        return START_REDELIVER_INTENT;// vuelve a la vida el servicio.


    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void writeNewUser(String usuario, String titulo, String descripcion, String pruebas, String tipo) {


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        String key = database.getReference("timeline").push().getKey();

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        long millis = new Date().getTime();

        Denuncias denuncias = new Denuncias(usuario, titulo, descripcion, pruebas, tipo, millis, false);

        mDatabase.child("timeline").child(key).setValue(denuncias);


        onDestroy();// Al finalizar transacción eliminar el servicio.
        this.stopSelf();

    }


    @Override
    public void onDestroy() { // destruir servicio
        Log.i(Tag.CLOUD_STORAGE, "servicio destruido");
        super.onDestroy();
    }

    @Override
    public boolean stopService(Intent name) {
        progreso = 0;
        return super.stopService(name);
    }
}
