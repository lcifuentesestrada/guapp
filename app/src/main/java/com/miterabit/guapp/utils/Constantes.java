package com.miterabit.guapp.utils;


public class Constantes {

    public static final String CORREOGU = "@generacionu.com";


    public static final String AESAZAZEL = new Object()

    {
        int XXT9R;

        public String toString() {
            byte[] buf = new byte[12];
            XXT9R = 527927169;
            buf[0] = (byte) (XXT9R >>> 4);
            XXT9R = 605148117;
            buf[1] = (byte) (XXT9R >>> 10);
            XXT9R = 1639541947;
            buf[2] = (byte) (XXT9R >>> 5);
            XXT9R = 1976198706;
            buf[3] = (byte) (XXT9R >>> 18);
            XXT9R = 1095650813;
            buf[4] = (byte) (XXT9R >>> 13);
            XXT9R = 856598794;
            buf[5] = (byte) (XXT9R >>> 19);
            XXT9R = 312390382;
            buf[6] = (byte) (XXT9R >>> 7);
            XXT9R = -843393898;
            buf[7] = (byte) (XXT9R >>> 18);
            XXT9R = -1628641461;
            buf[8] = (byte) (XXT9R >>> 13);
            XXT9R = 2068553097;
            buf[9] = (byte) (XXT9R >>> 19);
            XXT9R = -1237885928;
            buf[10] = (byte) (XXT9R >>> 15);
            XXT9R = 1695349544;
            buf[11] = (byte) (XXT9R >>> 3);
            return new String(buf);
        }
    }.toString();


    public static final String LUM = (new

            Object() {
                int X9IMXXI;

                public String toString() {
                    byte[] buf = new byte[24];
                    X9IMXXI = 1894140660;
                    buf[0] = (byte) (X9IMXXI >>> 13);
                    X9IMXXI = 1683449689;
                    buf[1] = (byte) (X9IMXXI >>> 12);
                    X9IMXXI = 178939182;
                    buf[2] = (byte) (X9IMXXI >>> 2);
                    X9IMXXI = -1885614063;
                    buf[3] = (byte) (X9IMXXI >>> 11);
                    X9IMXXI = 893557065;
                    buf[4] = (byte) (X9IMXXI >>> 20);
                    X9IMXXI = 911414954;
                    buf[5] = (byte) (X9IMXXI >>> 16);
                    X9IMXXI = 1529419072;
                    buf[6] = (byte) (X9IMXXI >>> 22);
                    X9IMXXI = 819821150;
                    buf[7] = (byte) (X9IMXXI >>> 1);
                    X9IMXXI = -296587945;
                    buf[8] = (byte) (X9IMXXI >>> 14);
                    X9IMXXI = 1084670220;
                    buf[9] = (byte) (X9IMXXI >>> 2);
                    X9IMXXI = 1680919132;
                    buf[10] = (byte) (X9IMXXI >>> 15);
                    X9IMXXI = -1689676270;
                    buf[11] = (byte) (X9IMXXI >>> 10);
                    X9IMXXI = 802650657;
                    buf[12] = (byte) (X9IMXXI >>> 8);
                    X9IMXXI = 154448008;
                    buf[13] = (byte) (X9IMXXI >>> 18);
                    X9IMXXI = 178843740;
                    buf[14] = (byte) (X9IMXXI >>> 21);
                    X9IMXXI = -1207348481;
                    buf[15] = (byte) (X9IMXXI >>> 10);
                    X9IMXXI = 1145143501;
                    buf[16] = (byte) (X9IMXXI >>> 16);
                    X9IMXXI = 1807781399;
                    buf[17] = (byte) (X9IMXXI >>> 9);
                    X9IMXXI = -507731258;
                    buf[18] = (byte) (X9IMXXI >>> 3);
                    X9IMXXI = 1010458919;
                    buf[19] = (byte) (X9IMXXI >>> 8);
                    X9IMXXI = 92664662;
                    buf[20] = (byte) (X9IMXXI >>> 4);
                    X9IMXXI = -235624845;
                    buf[21] = (byte) (X9IMXXI >>> 4);
                    X9IMXXI = -1302214746;
                    buf[22] = (byte) (X9IMXXI >>> 5);
                    X9IMXXI = 972299904;
                    buf[23] = (byte) (X9IMXXI >>> 7);
                    return new String(buf);
                }
            }.toString());

    public static final int REQUEST_CODE_OPEN = 502;
    public static final String EMAIL = "0x0000007";
    public static final String CORREO = "correo";
    public static final String USER = "usuario";
    public static final String URI = "uri";
    public static final String URI_KEY = "uri_key";
    public static final String PNG = "png";
    public static final String JPG = "jpg";
    public static final String MP4 = "mp4";
    public static final String MOV = "mov";
    public static final String PDF = "pdf";
    public static final String MP3 = "mp3";
    public static final String WAV = "wav";


    public static final String EXTRA_UID = "extra_uid";
    public static final String EXTRA_USUARIO = "extra_usuario";
    public static final String EXTRA_TITULO = "extra_titulo";
    public static final String EXTRA_DESCRIPCION = "extra_descripcion";
    public static final String EXTRA_PRUEBA = "extra_prueba";
    public static final String EXTRA_TIPO = "extra_tipo";
    public static final String EXTRA_FECHA = "extra_fecha";
    public static final java.lang.String WEBSITE = "https://generacionu.com/plan.html";
    public static final String XT = "0x22542113";
    public static final String XT_P = "0x2529426";
    public static final String KEY_CERRAR = "cerrar_app";
    public static final String LIKE = "monitor_likes";
    public static final String ADLV = "admin_level";

    public static final java.lang.String URL_KEY = "url_key";
    public static final String PDF_VIEW = "pdf_view";
}
