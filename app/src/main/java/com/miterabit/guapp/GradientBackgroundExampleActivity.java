package com.miterabit.guapp;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;

import java.util.ArrayList;
import java.util.List;

public class GradientBackgroundExampleActivity extends AhoyOnboarderActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AhoyOnboarderCard ahoyOnboarderCard1 = new AhoyOnboarderCard("GU", "Texto tutorial", R.drawable.ic_1);
        AhoyOnboarderCard ahoyOnboarderCard2 = new AhoyOnboarderCard("GU", "Texto tutorial", R.drawable.ic_2);
        AhoyOnboarderCard ahoyOnboarderCard3 = new AhoyOnboarderCard("GU", "Texto tutorial", R.drawable.ic_3);

        ahoyOnboarderCard1.setBackgroundColor(R.color.white);
        ahoyOnboarderCard2.setBackgroundColor(R.color.white);
        ahoyOnboarderCard3.setBackgroundColor(R.color.white);

        List<AhoyOnboarderCard> pages = new ArrayList<>();

        pages.add(ahoyOnboarderCard1);
        pages.add(ahoyOnboarderCard2);
        pages.add(ahoyOnboarderCard3);

        for (AhoyOnboarderCard page : pages) {
            page.setTitleColor(R.color.black);
            page.setDescriptionColor(R.color.grey_600);
        }

        setFinishButtonTitle("comenzar");
        setFinishButtonDrawableStyle(ContextCompat.getDrawable(this, R.drawable.rounded_button_tutorial));
        showNavigationControls(false);

        List<Integer> colorList = new ArrayList<>();
        colorList.add(R.color.colorPrimaryDark);
        colorList.add(R.color.colorPrimaryDark);
        colorList.add(R.color.colorPrimaryDark);
        setColorBackground(colorList);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/raleway.ttf");
        setFont(face);

        setOnboardPages(pages);

    }

    @Override
    public void onFinishButtonPressed() {
        finish();
    }
}
