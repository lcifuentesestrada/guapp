package com.miterabit.guapp.controlador;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.database.FirebaseDatabase;
import com.miterabit.guapp.R;
import com.miterabit.guapp.modelo.DenunciasSearch;
import com.miterabit.guapp.utils.Constantes;
import com.miterabit.guapp.utils.Tag;
import com.miterabit.guapp.vistas.auth.DetalleActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;


public class RecyclerViewAdapterList extends RecyclerView.Adapter<RecyclerViewAdapterList.ViewHolderList> {

    private static final String PICASSO = "picaso";
    private ArrayList<DenunciasSearch> values;
    private Context context;


    public RecyclerViewAdapterList(Context context, ArrayList<DenunciasSearch> values) {
        this.context = context;
        this.values = values;
    }


    @Override
    public RecyclerViewAdapterList.ViewHolderList onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timeline, parent, false);
        return new RecyclerViewAdapterList.ViewHolderList(view);


    }


    @Override
    public void onBindViewHolder(final RecyclerViewAdapterList.ViewHolderList holder, final int position) {

        SharedPreferences prefsUri = context.getSharedPreferences(Constantes.AESAZAZEL, Context.MODE_PRIVATE);
        boolean user = prefsUri.getBoolean(Constantes.ADLV, false);

        Log.i(Tag.TAG_MAIN, "actual: " + user);

        //final String key;


        if (user) {
            holder.aSwitch.setVisibility(View.VISIBLE);
            holder.aSwitch.setChecked(values.get(position).isEstado());

        } else {
            holder.aSwitch.setVisibility(View.INVISIBLE);
            holder.aSwitch.setChecked(values.get(position).isEstado());
        }

        holder.usuario.setText(values.get(position).getUsuario());
        holder.descrip.setText(values.get(position).getDescripcion());
        holder.titulo.setText(values.get(position).getTitulo());
        holder.fecha.setText(values.get(position).getFechacast());


        String tipo = values.get(position).getTipo();


        //Imagenes
        String png = Constantes.PNG;
        String jpg = Constantes.JPG;

        //video
        String MP4 = Constantes.MP4;
        String MOV = Constantes.MOV;
        //archivo
        String PDF = Constantes.PDF;
        //audio
        String MP3 = Constantes.MP3;
        String WAV = Constantes.WAV;

        switch (tipo) {

            //IMAGEN
            case Constantes.PNG:
                Log.i(Tag.ADAPTADOR, "png");
                Log.i(Tag.ADAPTADOR, "jpg");

                Picasso.with(context)
                        .load(values.get(position).getPruebas())
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .resize(512, 512)
                        .into(holder.imagen, new Callback() {
                            @Override
                            public void onSuccess() {

                                Picasso.with(context)
                                        .load(values.get(position).getPruebas())
                                        .error(R.drawable.ic_file)
                                        .resize(512, 512)
                                        .into(holder.imagen);
                            }

                            @Override
                            public void onError() {
                                try {
                                    Picasso.with(context)
                                            .load(values.get(position).getPruebas())
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .resize(512, 512)
                                            .into(holder.imagen);
                                } catch (Exception e) {
                                    Log.e(PICASSO, ": " + e);
                                }

                                Log.e(PICASSO, "No se pudo recuperar en cache");
                                Log.i(PICASSO, "Descargando...");
                            }
                        });

                break;
            case Constantes.JPG:

                Log.i(Tag.ADAPTADOR, "jpg");

                Picasso.with(context)
                        .load(values.get(position).getPruebas())
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .resize(512, 512)
                        .into(holder.imagen, new Callback() {
                            @Override
                            public void onSuccess() {

                                Picasso.with(context)
                                        .load(values.get(position).getPruebas())
                                        .error(R.drawable.ic_file)
                                        .resize(512, 512)
                                        .into(holder.imagen);
                            }

                            @Override
                            public void onError() {
                                try {
                                    Picasso.with(context)
                                            .load(values.get(position).getPruebas())
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .resize(512, 512)
                                            .into(holder.imagen);
                                } catch (Exception e) {
                                    Log.e(PICASSO, ": " + e);
                                }

                                Log.e(PICASSO, "No se pudo recuperar en cache");
                                Log.i(PICASSO, "Descargando...");
                            }
                        });


                break;
            //AUDIO
            case Constantes.WAV:
                holder.imagen.setBackground(context.getResources().getDrawable(R.drawable.ic_audio));
                Log.i(Tag.ADAPTADOR, "wav");
                break;
            case Constantes.MP3:
                holder.imagen.setBackground(context.getResources().getDrawable(R.drawable.ic_audio));
                Log.i(Tag.ADAPTADOR, "mp3");
                break;
            //PDF
            case Constantes.PDF:
                holder.imagen.setBackground(context.getResources().getDrawable(R.drawable.ic_pdf));
                Log.i(Tag.ADAPTADOR, "pdf");
                break;
            //VIDEO
            case Constantes.MP4:
                holder.imagen.setBackground(context.getResources().getDrawable(R.drawable.ic_video));
                Log.i(Tag.ADAPTADOR, "mp4");
                break;
            case Constantes.MOV:
                holder.imagen.setBackground(context.getResources().getDrawable(R.drawable.ic_video));
                Log.i(Tag.ADAPTADOR, "mov");
                break;
            default:
                holder.imagen.setBackground(context.getResources().getDrawable(R.drawable.ic_file));
                Log.i(Tag.ADAPTADOR, "otro formato");
                break;

        }


        holder.setItemClickListener((view, position1, isLongClick) -> {


            switch (view.getId()) {

                case R.id.estado_sw:


                    if (values.get(position).isEstado()) {
                        //Toast.makeText(context, "true", Toast.LENGTH_SHORT).show();
                        HashMap<String, Object> result = new HashMap<>();
                        result.put("estado", false);

                        String uid_key = values.get(position).getUid();

                        if (uid_key != null) {
                            FirebaseDatabase.getInstance().getReference().child("timeline").child(uid_key).updateChildren(result);
                        }

                    } else {
                        // Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                        HashMap<String, Object> result = new HashMap<>();
                        result.put("estado", true);

                        String uid_key = values.get(position).getUid();

                        if (uid_key != null) {
                            FirebaseDatabase.getInstance().getReference().child("timeline").child(uid_key).updateChildren(result);
                        }

                    }


                    break;

                case R.id.item_cardview:
                    detalle(position);
                    break;

                case R.id.item_uss:
                    detalle(position);
                    break;
                case R.id.item_ff:
                    detalle(position);
                    break;
                case R.id.item_textview_titulo:
                    detalle(position);
                    break;
                case R.id.item_edittext_descrip:
                    detalle(position);
                    break;
                case R.id.item_image:
                    detalle(position);
                    break;

                default:
                    detalle(position);
                    break;
            }

        });


    }

    private void detalle(int position) {
        Intent intentDetalleGridActivity = new Intent(context, DetalleActivity.class);
        intentDetalleGridActivity.putExtra(Constantes.EXTRA_UID, values.get(position).getUid());
        intentDetalleGridActivity.putExtra(Constantes.EXTRA_USUARIO, values.get(position).getUsuario());
        intentDetalleGridActivity.putExtra(Constantes.EXTRA_TITULO, values.get(position).getTitulo());
        intentDetalleGridActivity.putExtra(Constantes.EXTRA_DESCRIPCION, values.get(position).getDescripcion());
        intentDetalleGridActivity.putExtra(Constantes.EXTRA_PRUEBA, values.get(position).getPruebas());
        intentDetalleGridActivity.putExtra(Constantes.EXTRA_FECHA, values.get(position).getFechacast());
        intentDetalleGridActivity.putExtra(Constantes.EXTRA_TIPO, values.get(position).getTipo());
        intentDetalleGridActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION | FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intentDetalleGridActivity);
    }


    @Override
    public int getItemCount() {
        return values.size();
    }


    class ViewHolderList extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView usuario, fecha, titulo;
        private EditText descrip;
        private CardView cardView;
        private ImageView imagen;
        private Switch aSwitch;
        private ItemClickListener itemClickListener;

        public ViewHolderList(View itemView) {
            super(itemView);

            usuario = itemView.findViewById(R.id.item_uss);
            fecha = itemView.findViewById(R.id.item_ff);
            titulo = itemView.findViewById(R.id.item_textview_titulo);
            descrip = itemView.findViewById(R.id.item_edittext_descrip);
            aSwitch = itemView.findViewById(R.id.estado_sw);
            cardView = (CardView) itemView.findViewById(R.id.item_cardview);
            imagen = (ImageView) itemView.findViewById(R.id.item_image);
            // casoCerrado = (ImageView) itemView.findViewById(R.id.caso_cerrado);

            //ame.setOnClickListener(this);
            cardView.setOnClickListener(this);
            imagen.setOnClickListener(this);
            fecha.setOnClickListener(this);
            titulo.setOnClickListener(this);
            descrip.setOnClickListener(this);
            aSwitch.setOnClickListener(this);

//            casoCerrado.setOnClickListener(this);

        }


        public void setItemClickListener(ItemClickListener itemClickListener) {

            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {

            itemClickListener.onClick(view, getAdapterPosition(), false);


        }

        @Override
        public boolean onLongClick(View view) {

            itemClickListener.onClick(view, getAdapterPosition(), true);
            return false;
        }
    }

}
