package com.miterabit.guapp.controlador;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.miterabit.guapp.R;
import com.miterabit.guapp.modelo.ComentariosSearch;
import com.miterabit.guapp.modelo.DenunciasSearch;

import java.util.ArrayList;


public class RecyclerViewAdapterListComment extends RecyclerView.Adapter<RecyclerViewAdapterListComment.ViewHolderList> {


    private ArrayList<ComentariosSearch> values;
    private Context context;


    public RecyclerViewAdapterListComment(Context context, ArrayList<ComentariosSearch> values) {
        this.context = context;
        this.values = values;
    }


    @Override
    public RecyclerViewAdapterListComment.ViewHolderList onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comentario, parent, false);
        return new RecyclerViewAdapterListComment.ViewHolderList(view);


    }


    @Override
    public void onBindViewHolder(final RecyclerViewAdapterListComment.ViewHolderList holder, final int position) {

        //final String key;

        holder.usuario.setText(values.get(position).getUsuario());
        holder.descrip.setText(values.get(position).getComentario());
        holder.fecha.setText(values.get(position).getFechacast());


    }


    @Override
    public int getItemCount() {
        return values.size();
    }


    class ViewHolderList extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView usuario, fecha;
        private EditText descrip;

        private ItemClickListener itemClickListener;

        public ViewHolderList(View itemView) {
            super(itemView);

            usuario = itemView.findViewById(R.id.item_uss);
            fecha = itemView.findViewById(R.id.item_ff);
            descrip = itemView.findViewById(R.id.item_comm);


        }


        public void setItemClickListener(ItemClickListener itemClickListener) {

            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {

            itemClickListener.onClick(view, getAdapterPosition(), false);


        }

        @Override
        public boolean onLongClick(View view) {

            itemClickListener.onClick(view, getAdapterPosition(), true);
            return false;
        }
    }

}
