package com.miterabit.guapp.dialogException;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.miterabit.guapp.R;



public class CuadroDialogo {


    public CuadroDialogo(Context context) {

          final Dialog dialog = new Dialog(context);
          dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
          dialog.setCancelable(true);
          dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
          dialog.setContentView(R.layout.dialog_exception_login);
          dialog.show();

    }



}
