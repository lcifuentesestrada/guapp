package com.miterabit.guapp.dialogException;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.miterabit.guapp.R;
import com.miterabit.guapp.modelo.Comentarios;
import com.miterabit.guapp.utils.Constantes;

import java.util.Date;


public class DialogAddComment {

    private FirebaseAuth mAuth;
    EditText comentario;
    FloatingActionButton floatingActionButton;

    public DialogAddComment(Context context, String uid) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_comment);
        dialog.show();


        floatingActionButton = dialog.findViewById(R.id.fab_add_comment);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                comentario = dialog.findViewById(R.id.edittext_add_comment);

                mAuth = FirebaseAuth.getInstance();

                //usuario
                SharedPreferences prefsUri = context.getSharedPreferences(Constantes.EMAIL, Context.MODE_PRIVATE);
                String user = prefsUri.getString(Constantes.USER, null);

                //correo
                String sEmail = mAuth.getCurrentUser().getEmail();

                //Comentario
                String sComentario = comentario.getText().toString();


                writeNewUser(user, sEmail, sComentario, uid, dialog);


            }
        });


    }

    private void writeNewUser(String user, String sEmail, String sComentario, String uid, Dialog dialog) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        String key = database.getReference("timeline").push().getKey();

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        long millis = new Date().getTime();

        Comentarios comentarios = new Comentarios(user, sEmail, sComentario, millis);

        mDatabase.child("timeline").child(uid).child("comentarios").child(key).setValue(comentarios);


        dialog.dismiss();
    }


}
