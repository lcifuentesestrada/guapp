package com.miterabit.guapp.dialogException;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.miterabit.guapp.R;

/**
 * Created by cifue on 18/03/2018.
 */

public class QRDialogException {

    public QRDialogException(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_exception_qr);
        dialog.show();


        Button button_returnqr = (Button) dialog.findViewById(R.id.salir_sos);
        button_returnqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

}
