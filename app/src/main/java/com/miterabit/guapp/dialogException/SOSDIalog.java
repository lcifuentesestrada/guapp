package com.miterabit.guapp.dialogException;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.Button;

import com.miterabit.guapp.R;


public class SOSDIalog {

    public SOSDIalog(Context context) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);

        dialog.setContentView(R.layout.dialog_sos);
        dialog.show();


        Button salir = dialog.findViewById(R.id.salir_sos);

        salir.setOnClickListener(view -> {
            dialog.dismiss();
        });


    }

}
