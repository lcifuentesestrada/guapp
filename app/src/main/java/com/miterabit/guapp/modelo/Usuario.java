package com.miterabit.guapp.modelo;

/**
 * Created by cifue on 18/03/2018.
 */

public class Usuario {

    private String usuario;
    private String correo;
    private String nombre;
    private String cui;
    private String carnet;
    private long timestamp;
    private boolean baneado;

    public Usuario() {
    }


    public Usuario(String usuario, String correo, String nombre, String cui, String carnet, long timestamp, boolean baneado) {
        this.usuario = usuario;
        this.correo = correo;
        this.nombre = nombre;
        this.cui = cui;
        this.carnet = carnet;
        this.timestamp = timestamp;
        this.baneado = baneado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCui() {
        return cui;
    }

    public void setCui(String cui) {
        this.cui = cui;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isBaneado() {
        return baneado;
    }

    public void setBaneado(boolean baneado) {
        this.baneado = baneado;
    }
}
