package com.miterabit.guapp.modelo;


public class EstadoDenuncia {



    private boolean estado;


    public EstadoDenuncia() {
    }

    public EstadoDenuncia(boolean estado) {
        this.estado = estado;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }



}
