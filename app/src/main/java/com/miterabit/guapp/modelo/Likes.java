package com.miterabit.guapp.modelo;

/**
 * Created by cifue on 26/03/2018.
 */

public class Likes {

    private int like;
    private String correo;
    private String usuario;


    public Likes() {

    }

    public Likes(int like, String correo, String usuarioid) {
        this.like = like;
        this.correo = correo;
        this.usuario = usuarioid;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
