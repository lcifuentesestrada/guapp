package com.miterabit.guapp.modelo;



public class ComentariosSearch {

    private String usuario;
    private String correo;
    private String comentario;
    private long timestamp;
    private String fechacast;


    public ComentariosSearch() {

    }

    public ComentariosSearch(String usuario, String correo, String comentario, long timestamp, String fechacast) {
        this.usuario = usuario;
        this.correo = correo;
        this.comentario = comentario;
        this.timestamp = timestamp;
        this.fechacast = fechacast;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getFechacast() {
        return fechacast;
    }

    public void setFechacast(String fechacast) {
        this.fechacast = fechacast;
    }
}
