package com.miterabit.guapp.modelo;

/**
 * Created by cifue on 22/03/2018.
 */

public class DenunciasSearch {

    private String usuario;
    private String titulo;
    private String descripcion;
    private String pruebas;
    private String tipo;
    private long timestamp;
    private String fechacast;
    private String uid;
    private boolean estado;


    public DenunciasSearch() {
    }

    public DenunciasSearch(String usuario, String titulo, String descripcion, String pruebas, String tipo, long timestamp, String fechacast, String uid, boolean estado) {
        this.usuario = usuario;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.pruebas = pruebas;
        this.tipo = tipo;
        this.timestamp = timestamp;
        this.fechacast = fechacast;
        this.uid = uid;
        this.estado = estado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPruebas() {
        return pruebas;
    }

    public void setPruebas(String pruebas) {
        this.pruebas = pruebas;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getFechacast() {
        return fechacast;
    }

    public void setFechacast(String fechacast) {
        this.fechacast = fechacast;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}


