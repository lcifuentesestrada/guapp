package com.miterabit.guapp.modelo;


public class Denuncias {


    private String usuario;
    private String titulo;
    private String descripcion;
    private String pruebas;
    private String tipo;
    private long timestamp;
    private boolean estado;

    public Denuncias() {
    }

    public Denuncias(String usuario, String titulo, String descripcion, String pruebas, String tipo, long timestamp, boolean estado) {
        this.usuario = usuario;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.pruebas = pruebas;
        this.tipo = tipo;
        this.timestamp = timestamp;
        this.estado = estado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPruebas() {
        return pruebas;
    }

    public void setPruebas(String pruebas) {
        this.pruebas = pruebas;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }


    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}

