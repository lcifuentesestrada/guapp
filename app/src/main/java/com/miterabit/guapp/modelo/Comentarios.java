package com.miterabit.guapp.modelo;



public class Comentarios {

    private String usuario;
    private String correo;
    private String comentario;
    private long timestamp;


    public Comentarios() {

    }

    public Comentarios(String usuario, String correo, String comentario, long timestamp) {
        this.usuario = usuario;
        this.correo = correo;
        this.comentario = comentario;
        this.timestamp = timestamp;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
