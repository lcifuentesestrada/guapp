package com.miterabit.guapp;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.miterabit.guapp.dialogException.CuadroDialogo;
import com.miterabit.guapp.moduloQR.QrScanner;
import com.miterabit.guapp.utils.Tag;
import com.miterabit.guapp.vistas.auth.Timeline;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    /**
     * Saber la lista
     */

    ArrayList<String> nombre = new ArrayList<>();
    ArrayList<String> cui = new ArrayList<>();
    ArrayList<String> carnet = new ArrayList<>();


    protected EditText usuario;
    protected EditText password;
    protected Context context;

    /**
     * Auth
     */

    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tutorial();

        mAuth = FirebaseAuth.getInstance();


        ConstraintLayout constraintLayout = (ConstraintLayout) findViewById(R.id.constraint_main);
        constraintLayout.getBackground().setAlpha(255);

        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);

        context = this;




    }

    private void updateUI(FirebaseUser currentUser) {
        //si esta autenticado.
        Log.i(Tag.TAG_MAIN, "" + currentUser);
        if (currentUser != null) {
            startActivity(new Intent(this, Timeline.class));
            this.finish();
        }

    }


    public void iniciarSession(View view) {

        // usuario = (EditText) findViewById(R.id.login_edit_usuario);
        // password = (EditText) findViewById(R.id.login_edit_pass);

        if (usuario.getText().toString().isEmpty()) {
            usuario.setError("usuario requerido");

        }

        if (password.getText().toString().isEmpty()) {
            password.setError("contraseña requerida");
        }


        if (!usuario.getText().toString().isEmpty() && !password.getText().toString().isEmpty()) {


            new CuadroDialogo(context);
            validarFirebase(usuario.getText().toString(), password.getText().toString());


        }


    }

    private void validarFirebase(String email, String password) {
/*
        mAuth.createUserWithEmailAndPassword(usuario, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(Tag.TAG_MAIN, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            Toast.makeText(MainActivity.this, "correo o contraseña erroneas", Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                    }
                });*/

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(Tag.TAG_MAIN, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.e(Tag.TAG_MAIN, "signInWithEmail:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });


    }

    public void fabLecturaQR(View view) {


        startActivity(new Intent(this, QrScanner.class));
        this.finish();


    }


    private void tutorial() {

        Boolean isFirstRun = getSharedPreferences("PREFERENCE_INIT", MODE_PRIVATE).getBoolean("isFirstRun", true);
        if (isFirstRun) {
            startActivity(new Intent(this, GradientBackgroundExampleActivity.class));
            //Toast.makeText(InicioSesion.this, "First Run", Toast.LENGTH_LONG).show();

        }
        getSharedPreferences("PREFERENCE_INIT", MODE_PRIVATE).edit().putBoolean("isFirstRun", false).apply();

    }


}
