package com.miterabit.guapp.vistas.auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.miterabit.guapp.R;
import com.miterabit.guapp.utils.Constantes;
import com.miterabit.guapp.utils.Tag;


public class UrlEmbed extends AppCompatActivity {

    WebView mWebview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_url_embed);


        Intent intentEntrante = getIntent();
        Bundle b = intentEntrante.getExtras();
        if (b != null) {
            String url = b.getString(Constantes.URL_KEY);

            mWebview = findViewById(R.id.webview_embed);

            //mWebview.loadData("<iframe src='https://generacionu.com/' style='border: 0; width: 100%; height: 100%'></iframe>", "text/html; charset=utf-8", "UTF-8");
            WebSettings webSettings = mWebview.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setUseWideViewPort(true);
            webSettings.setLoadWithOverviewMode(true);

            //mWebview.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

            final Activity activity = this;

            mWebview.setWebViewClient(new WebViewClient() {
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
                }


            });


            Log.d(Tag.WEBVIEW, "url: " + url);
            mWebview.loadUrl(url);


        }


    }

    @Override
    public void onPause() {
        mWebview.onPause();
        mWebview.pauseTimers();
        super.onPause();
    }

    @Override
    public void onResume() {
        mWebview.resumeTimers();
        mWebview.onResume();
        super.onResume();
    }


    @Override
    protected void onDestroy() {
        mWebview.destroy();
        mWebview = null;
        super.onDestroy();
    }


    public void finishWebView(View view) {

        finish();


    }
}
