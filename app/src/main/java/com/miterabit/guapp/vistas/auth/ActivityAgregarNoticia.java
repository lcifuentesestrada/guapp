package com.miterabit.guapp.vistas.auth;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.miterabit.guapp.R;
import com.miterabit.guapp.utils.Constantes;
import com.miterabit.guapp.utils.FirebaseUpload;
import com.miterabit.guapp.utils.Tag;

import java.io.File;
import java.util.concurrent.ThreadLocalRandom;

import static com.miterabit.guapp.utils.Constantes.REQUEST_CODE_OPEN;

public class ActivityAgregarNoticia extends AppCompatActivity {

    int progress = 0;
    int identificador;
    private Handler handler;
    private Runnable runnable;
    private final int runTime = 1000;
    NotificationManager notificationManager;
    Notification.Builder notificationBuilder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_noticia);
        FloatingActionButton publicar = findViewById(R.id.agregar_fab_publicar);
        //  publicar.setEnabled(false);
        //  publicar.setBackgroundColor(Color.parseColor("#1F000000"));
        vaciarUri();
    }


    public void obtenerUri(View view) {


        EditText titulo = findViewById(R.id.agregar_edittext_titulo);
        EditText descrip = findViewById(R.id.agregar_edittext_descrip);

        if (titulo.getText().toString().isEmpty() | descrip.getText().toString().isEmpty()) {

            titulo.setError(getResources().getString(R.string.add_titulo));
            descrip.setError(getResources().getString(R.string.add_descrip));

        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            String[] mimetypes = {"image/*", "video/*", "audio/*", "application/pdf/*"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
            startActivityForResult(intent, REQUEST_CODE_OPEN);

        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_OPEN) {

            if (resultCode == RESULT_OK) {


                //obtener uri del archivo
                Uri uri = data.getData();
                Log.i(Tag.DENUNCIA, "uri de archivo: " + uri);

                SharedPreferences prefs = getSharedPreferences(Constantes.URI, Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(Constantes.URI_KEY, String.valueOf(uri));
                editor.apply();


            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void iniciarNotificationDialog(final int id_notifi) {

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

                Log.d(Tag.CLOUD_STORAGE, "progreso antes de volver a entrar: " + progress);
                progress = FirebaseUpload.getProgreso();

                Log.i(Tag.CLOUD_STORAGE, "runnable agregar noticia: " + progress);
                Log.d(Tag.CLOUD_STORAGE, "id: " + id_notifi);
                Log.d(Tag.CLOUD_STORAGE, "progreso despues de entrar: " + progress);
                handler.postDelayed(runnable, runTime);
                Integer notificationID = id_notifi;
                notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationBuilder = new Notification.Builder(getApplicationContext());
                notificationBuilder
                        .setContentTitle("Subiendo prueba...")
                        .setContentText(progress + "% " + "completo")
                        .setSmallIcon(R.drawable.icon)
                        .setOngoing(true)
                        .setProgress(100, progress, false);

                Notification notification = notificationBuilder.build();
                notificationManager.notify(notificationID, notification);


                if (progress == 100) {

                    Log.i(Tag.CLOUD_STORAGE, "thread finalizado");
                    handler.removeCallbacks(runnable);
                    notificationManager.cancel(id_notifi);
                    Intent myService = new Intent(ActivityAgregarNoticia.this, FirebaseUpload.class);
                    stopService(myService);

                }
            }
        };
        handler.post(runnable);
    }


    public static String getMimeType(Context context, Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }


    public void publicarDenuncia(View view) {
        SharedPreferences prefsUriv = getSharedPreferences(Constantes.URI, Context.MODE_PRIVATE);
        String URIv = prefsUriv.getString(Constantes.URI_KEY, null);

        Log.i(Tag.DENUNCIA, "publicar uri: " + URIv);

        if (URIv != null) {


            //obtener titulo y descripcion

            EditText etitulo = findViewById(R.id.agregar_edittext_titulo);
            EditText edescrip = findViewById(R.id.agregar_edittext_descrip);

            String extra_titulo = etitulo.getText().toString();
            String extra_descrip = edescrip.getText().toString();

            //obtener el correo logeado


            SharedPreferences prefcorreo = getSharedPreferences(Constantes.EMAIL, Context.MODE_PRIVATE);
            String correo = prefcorreo.getString(Constantes.CORREO, null);


            SharedPreferences prefsu = getSharedPreferences(Constantes.EMAIL, Context.MODE_PRIVATE);
            String usuario = prefsu.getString(Constantes.USER, null);
            //extencion del archivo


            SharedPreferences prefsUri = getSharedPreferences(Constantes.URI, Context.MODE_PRIVATE);
            String URI = prefsUri.getString(Constantes.URI_KEY, null);


            String exnt = getMimeType(this, Uri.parse(URI));

            if (correo != null) {
                Log.i(Tag.DENUNCIA, "intent ");
                Intent i = new Intent(this, FirebaseUpload.class);
                i.putExtra("data", URI); // enviar uri
                i.putExtra("correo", correo); // enviar correo.
                i.putExtra("titulo", extra_titulo);
                i.putExtra("descripcion", extra_descrip);
                i.putExtra("extencion", exnt);
                i.putExtra("usuario", usuario);

                startService(i);

                //random id notification

                int randomNum = ThreadLocalRandom.current().nextInt(2, 250 + 1);
                Log.i(Tag.CLOUD_STORAGE, "random: " + randomNum);
                iniciarNotificationDialog(randomNum);
                Button lock = findViewById(R.id.agregar_button_prueba);
                lock.setEnabled(false);
                lock.invalidate();
                lock.setBackgroundColor(getResources().getColor(R.color.common_google_signin_btn_text_dark_disabled));

            } else {
                Toast.makeText(this, "occurio un error al seleccionar", Toast.LENGTH_SHORT).show();
            }

            vaciarUri();
            finish();

        } else {
            Snackbar.make(view, "Seleccione una prueba", Snackbar.LENGTH_LONG).show();
        }


    }

    private void vaciarUri() {
        SharedPreferences prefs = getSharedPreferences(Constantes.URI, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constantes.URI_KEY, null);
        editor.apply();
    }

    @Override
    public void finish() {
        progress = 0;
        super.finish();

    }


    public void fabExit(View view) {

        finish();

    }

    @Override
    public void onBackPressed() {


        finish();


    }


}
