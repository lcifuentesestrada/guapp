package com.miterabit.guapp.vistas.auth;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.miterabit.guapp.R;
import com.miterabit.guapp.controlador.RecyclerViewAdapterListComment;
import com.miterabit.guapp.dialogException.DialogAddComment;
import com.miterabit.guapp.modelo.Comentarios;
import com.miterabit.guapp.modelo.ComentariosSearch;
import com.miterabit.guapp.modelo.Likes;
import com.miterabit.guapp.utils.Constantes;
import com.miterabit.guapp.utils.DownloadTaskPDF;
import com.miterabit.guapp.utils.Tag;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

public class DetalleActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private FirebaseAuth mAuth;
    TextView tvtitulo, tvusuario, tvfecha;
    EditText eddescripcion;

    ArrayList<String> usuario = new ArrayList<>();
    ArrayList<String> comentario = new ArrayList<>();
    ArrayList<String> fecha = new ArrayList<>();

    int i = 0;
    long ct = 0;

    private Handler handler;
    private Runnable runnable;
    private final int runTime = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_new);


        mAuth = FirebaseAuth.getInstance();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_comentarios);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView.setLayoutManager(layoutManager);


        Intent intentEntrante = getIntent();
        Bundle b = intentEntrante.getExtras();
        if (b != null) {


            ImageView imagen = findViewById(R.id.imagen_detalle);


            String uid = (String) b.get(Constantes.EXTRA_UID);
            String usuario = (String) b.get(Constantes.EXTRA_USUARIO);
            String titulo = (String) b.get(Constantes.EXTRA_TITULO);
            String descripcion = (String) b.get(Constantes.EXTRA_DESCRIPCION);
            String pruebas = (String) b.get(Constantes.EXTRA_PRUEBA);
            String fecha = (String) b.get(Constantes.EXTRA_FECHA);
            String tipo = (String) b.get(Constantes.EXTRA_TIPO);


            Log.i(Tag.DETALLE, "uid: " + uid);
            Log.i(Tag.DETALLE, "usuario: " + usuario);
            Log.i(Tag.DETALLE, "titulo: " + titulo);
            Log.i(Tag.DETALLE, "descripcion: " + descripcion);
            Log.i(Tag.DETALLE, "pruebas url :" + pruebas);
            Log.i(Tag.DETALLE, "fecha cast: " + fecha);
            Log.i(Tag.DETALLE, "tipo: " + tipo);


            tvtitulo = findViewById(R.id.detalle_textview_titulo);
            tvusuario = findViewById(R.id.detalle_textview_autor);
            tvfecha = findViewById(R.id.detalle_textview_fecha);
            eddescripcion = findViewById(R.id.detalle_edittext_parrafo);

            tvtitulo.setText(titulo);
            tvusuario.setText(usuario);
            tvfecha.setText(fecha);
            eddescripcion.setText(descripcion);

            cargarComentarios(uid);
            verificarLike(uid);

            cargarLikes(uid);

            estadoPublicacion(uid);


            if (tipo != null) {
                switch (tipo) {

                    //IMAGEN
                    case Constantes.PNG:
                        loadImage(pruebas, imagen);
                        break;
                    case Constantes.JPG:

                        loadImage(pruebas, imagen);
                        break;
                    //AUDIO
                    case Constantes.WAV:
                        imagen.setBackground(getResources().getDrawable(R.drawable.ic_audio));
                        Log.i(Tag.ADAPTADOR, "wav");
                        break;
                    case Constantes.MP3:
                        imagen.setBackground(getResources().getDrawable(R.drawable.ic_audio));
                        Log.i(Tag.ADAPTADOR, "mp3");
                        break;
                    //PDF
                    case Constantes.PDF:
                        imagen.setBackground(getResources().getDrawable(R.drawable.ic_pdf));
                        Log.i(Tag.ADAPTADOR, "pdf");
                        break;
                    //VIDEO
                    case Constantes.MP4:
                        imagen.setBackground(getResources().getDrawable(R.drawable.ic_video));
                        Log.i(Tag.ADAPTADOR, "mp4");
                        break;
                    case Constantes.MOV:
                        imagen.setBackground(getResources().getDrawable(R.drawable.ic_video));
                        Log.i(Tag.ADAPTADOR, "mov");
                        break;
                    default:
                        imagen.setBackground(getResources().getDrawable(R.drawable.ic_file));
                        Log.i(Tag.ADAPTADOR, "otro formato");
                        break;

                }
            }


        }

        Resources res = getResources();

        TabHost tabs = (TabHost) findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec = tabs.newTabSpec("mitab1");
        spec.setContent(R.id.Descripción);
        spec.setIndicator("Descripción", res.getDrawable(android.R.drawable.ic_btn_speak_now));
        tabs.addTab(spec);

        spec = tabs.newTabSpec("mitab2");
        spec.setContent(R.id.comentarios);
        spec.setIndicator("comentarios", res.getDrawable(android.R.drawable.ic_dialog_map));
        tabs.addTab(spec);

        tabs.setCurrentTab(0);


    }

    private void loadImage(String pruebas, ImageView imagen) {
        Log.i(Tag.ADAPTADOR, "png");
        Log.i(Tag.ADAPTADOR, "jpg");

        Picasso.with(this)
                .load(pruebas)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .resize(512, 512)
                .into(imagen, new Callback() {
                    @Override
                    public void onSuccess() {

                        Picasso.with(getApplicationContext())
                                .load(pruebas)
                                .error(R.drawable.ic_file)
                                .resize(512, 512)
                                .into(imagen);
                    }

                    @Override
                    public void onError() {
                        try {
                            Picasso.with(getApplicationContext())
                                    .load(pruebas)
                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                    .resize(512, 512)
                                    .into(imagen);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                });

    }

    private void estadoPublicacion(String uid) {


        new DetalleActivity.GetDataFromFirebase().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        // Read from the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("timeline").child(uid).child("estado");


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                Boolean estado = dataSnapshot.getValue(Boolean.class);

                Log.d(Tag.COMENTAR, "" + estado);

                if (estado != null) {

                    if (estado) {
                        FloatingActionButton floatingActionButton = findViewById(R.id.fab_comentar);
                        floatingActionButton.setVisibility(View.INVISIBLE);

                    } else {
                        FloatingActionButton floatingActionButton = findViewById(R.id.fab_comentar);
                        floatingActionButton.setVisibility(View.VISIBLE);
                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void cargarLikes(String uid) {


        new DetalleActivity.GetDataFromFirebase().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        // Read from the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("timeline/" + uid + "/likes");


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ct = dataSnapshot.getChildrenCount();

                TextView tv = findViewById(R.id.text_megusta);
                String s = String.valueOf(ct);

                tv.setText(s);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void verificarLike(String UID_COMENTARIO) {

        SharedPreferences prefsUri = getSharedPreferences(Constantes.EMAIL, Context.MODE_PRIVATE);
        String user = prefsUri.getString(Constantes.USER, null);

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        Query query = reference.child("timeline/" + UID_COMENTARIO + "/likes").orderByChild("usuario").equalTo(user);
        Log.i(Constantes.LIKE, "comentarios/" + UID_COMENTARIO + "/likes");

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i(Constantes.LIKE, "" + dataSnapshot);
                if (dataSnapshot.exists()) {

                    FloatingActionButton floatingActionButton = findViewById(R.id.floatingActionButtonAddLike);
                    floatingActionButton.getDrawable().mutate().setTint(getResources().getColor(R.color.colorPrimary));

                    i = 1;


                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(Tag.TAG_AUTH_REGISTER, "ERROR: " + databaseError);
            }


        });


    }

    private void cargarComentarios(String uid) {


        new DetalleActivity.GetDataFromFirebase().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        // Read from the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("timeline").child(uid).child("comentarios");


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                usuario.clear();
                comentario.clear();
                fecha.clear();


                if (!dataSnapshot.exists()) {
                    usuario.add("No hay comentarios");
                    comentario.add("agrega un comentario");
                    long f = 1000000000;
                    fecha.add(getDate(f));
                }

                for (DataSnapshot detalle : dataSnapshot.getChildren()) {
                    //  System.out.println(alert.getValue());
                    Comentarios listado = detalle.getValue(Comentarios.class);
                    Log.i(Tag.KEY_DETALLE_FINAL, "|-------------------------DENUNCIA------------------------|--->");
                    Log.i(Tag.KEY_DETALLE_FINAL, "|                                                         |");
                    Log.i(Tag.KEY_DETALLE_FINAL, "|                                                         |");
                    Log.i(Tag.KEY_DETALLE_FINAL, "key_detalle: " + detalle.getKey());
                    Log.i(Tag.KEY_DETALLE_FINAL, "descripcion: " + listado.getUsuario());
                    Log.i(Tag.KEY_DETALLE_FINAL, "pruebas: " + listado.getComentario());
                    Log.i(Tag.KEY_DETALLE_FINAL, "tipo: " + listado.getCorreo());
                    Log.i(Tag.KEY_DETALLE_FINAL, "tipo: " + listado.getTimestamp());
                    Log.i(Tag.KEY_DETALLE_FINAL, "|                                                         |");
                    Log.i(Tag.KEY_DETALLE_FINAL, "|                                                         |");
                    Log.i(Tag.KEY_DETALLE_FINAL, "|---------------------------------------------------------|--->");


                    Log.d(Tag.DETALLE, "sin comentarios ->" + detalle);


                    usuario.add(listado.getUsuario());
                    comentario.add(listado.getComentario());
                    fecha.add(getDate(listado.getTimestamp()));


                }
                ArrayList androidVersions = prepareData();
                RecyclerViewAdapterListComment adapter = new RecyclerViewAdapterListComment(getApplicationContext(), androidVersions);
                recyclerView.setAdapter(adapter);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private ArrayList prepareData() {

        ArrayList android_version = new ArrayList<>();
        for (int i = 0; i < usuario.size(); i++) {
            ComentariosSearch androidVersion = new ComentariosSearch();
            androidVersion.setUsuario(usuario.get(i));
            androidVersion.setComentario(comentario.get(i));
            androidVersion.setFechacast(fecha.get(i));
            android_version.add(androidVersion);

        }
        return android_version;
    }

    public void agregarlike(View view) {


        if (i == 1) {

            SharedPreferences prefsUri = getSharedPreferences(Constantes.EMAIL, Context.MODE_PRIVATE);
            String user = prefsUri.getString(Constantes.USER, null);

            //uid usuario
            String sUID = mAuth.getCurrentUser().getUid();

            Intent intentEntrante = getIntent();
            Bundle b = intentEntrante.getExtras();
            String uid = (String) b.get(Constantes.EXTRA_UID);

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            Query applesQuery = null;
            if (uid != null) {
                applesQuery = ref.child("timeline").child(uid).child("likes").child(sUID);
            }

            if (applesQuery != null) {
                applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        Log.i(Constantes.LIKE, "" + dataSnapshot);
                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                            appleSnapshot.getRef().removeValue();
                            FloatingActionButton floatingActionButton = view.findViewById(R.id.floatingActionButtonAddLike);
                            floatingActionButton.getDrawable().mutate().setTint(getResources().getColor(R.color.color_gradient));


                            i = 0;

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.i(Constantes.LIKE, "" + databaseError);
                    }
                });
            }

        } else if (i == 0) {

            Intent intentEntrante = getIntent();
            Bundle b = intentEntrante.getExtras();
            if (b != null) {


                String uid = (String) b.get(Constantes.EXTRA_UID);


                mAuth = FirebaseAuth.getInstance();
                //usuario
                SharedPreferences prefsUri = getSharedPreferences(Constantes.EMAIL, Context.MODE_PRIVATE);
                String user = prefsUri.getString(Constantes.USER, null);

                //correo
                String sEmail = mAuth.getCurrentUser().getEmail();

                //uid usuario
                String sUID = mAuth.getCurrentUser().getUid();


                writeLike(user, sEmail, uid, sUID, view);

                FloatingActionButton floatingActionButton = view.findViewById(R.id.floatingActionButtonAddLike);
                floatingActionButton.getDrawable().mutate().setTint(getResources().getColor(R.color.colorPrimary));

            }
        }


    }

    public void verPruebas(View view) {
        //cargar webview con la url


        Intent intentEntrante = getIntent();
        Bundle b = intentEntrante.getExtras();

        if (b != null) {

            String tipo = (String) b.get(Constantes.EXTRA_TIPO);
            String pruebas = (String) b.get(Constantes.EXTRA_PRUEBA);

            String pdf = "pdf";

            if (Objects.equals(tipo, pdf)) {

                if (verficarPermisos()) {

                    new DownloadTaskPDF(this, pruebas);


                }

                if (!verficarPermisos()) {
                    Snackbar.make(view, "Se necesitan permisos de almacenamiento para ver un PDF", Snackbar.LENGTH_LONG).show();
                }

            } else {
                Log.d("Download Task", "nada");
                Intent webview = new Intent(this, UrlEmbed.class);
                webview.putExtra(Constantes.URL_KEY, pruebas);
                startActivity(webview);
            }


        }


    }

    @SuppressLint("StaticFieldLeak")
    private class GetDataFromFirebase extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }

    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        return DateFormat.format("dd-MM-yyyy", cal).toString();
    }


    public void finish(View view) {
        this.finish();
    }


    public void showDialogAddComment(View view) {


        Intent intentEntrante = getIntent();
        Bundle b = intentEntrante.getExtras();

        if (b != null) {
            String uid = (String) b.get(Constantes.EXTRA_UID);

            new DialogAddComment(this, uid);
        }
    }

    private void writeLike(String user, String correo, String uid, String USERUID, View view) {


        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();


        Likes likes = new Likes(1, correo, user);

        mDatabase.child("timeline").child(uid).child("likes").child(USERUID).setValue(likes);

        i = 1;


    }


    private boolean verficarPermisos() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionChecke = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED && permissionChecke != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
            }, 225);
        } else {
            return true;
        }
        return false;
    }

}
