package com.miterabit.guapp.vistas.auth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.miterabit.guapp.R;
import com.miterabit.guapp.modelo.Usuario;
import com.miterabit.guapp.utils.Constantes;
import com.miterabit.guapp.utils.Tag;
import com.scottyab.aescrypt.AESCrypt;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Date;

public class RegistrarUsuario extends AppCompatActivity {

    protected EditText usuario;
    protected Button auth;


    ArrayList<String> igualesArray = new ArrayList<>();

    private FirebaseAuth mAuth;


    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }


    private void updateUI(FirebaseUser user) {

        if (user != null) {
            startActivity(new Intent(this, Timeline.class));

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_usuario);
        mAuth = FirebaseAuth.getInstance();
        igualesArray.clear();

/*
        usuario = (EditText) findViewById(R.id.registrar_edit_usuario);
        password = (EditText) findViewById(R.id.registrar_edit_pass);
        correo = (EditText) findViewById(R.id.registrar_edit_correo);
        nombre = (EditText) findViewById(R.id.registrar_edit_nombre);
        cui = (EditText) findViewById(R.id.registrar_edit_cui);
        carnet = (EditText) findViewById(R.id.registrar_edit_carnet);
*/

        usuario = (EditText) findViewById(R.id.registrar_editText_usuario);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            String snombre = bundle.getString(Tag.EXTRA_NOMBRE);
            String scui = bundle.getString(Tag.EXTRA_CUI);
            String scarnet = bundle.getString(Tag.EXTRA_CARNET);

            //  nombre.setText(snombre);
            //  cui.setText(scui);
            // carnet.setText(scarnet);


        }

        //   auth = (Button) findViewById(R.id.registrar_button_registrar);
        //  auth.setOnClickListener(this);

    }


    public void onClick(View view) {


    }

    private void ingresar(final View view) {

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            String snombre = bundle.getString(Tag.EXTRA_NOMBRE);
            String scui = bundle.getString(Tag.EXTRA_CUI);
            String scarnet = bundle.getString(Tag.EXTRA_CARNET);

            //  nombre.setText(snombre);
            //  cui.setText(scui);
            // carnet.setText(scarnet);


            final String saUsuario = usuario.getText().toString();
            final String saCorreo = scarnet + Constantes.CORREOGU;
            final String saPass = scui + scarnet + Constantes.AESAZAZEL;
            final String saNombre = snombre;
            final String saCui = scui;
            final String saCarnet = scarnet;

            if (validarEditTextnull()) {

                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                Query query = reference.child("usuarios").orderByChild("usuario").equalTo(saUsuario);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            Snackbar.make(view, "El nombre público ya esta en uso", Snackbar.LENGTH_LONG)
                                    .show();

                        } else {

                            registrar(saCorreo, saPass, saUsuario, saNombre, saCui, saCarnet);

                        }
                    }


                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                        Log.e(Tag.TAG_AUTH_REGISTER, "ERROR: " + databaseError);
                    }


                });


            }

        } else {
            Log.d(Tag.TAG_AUTH_REGISTER, "faltan datos");
        }

    }

    private void registrar(final String saCorreo, String saPass, final String saUsuario, final String saNombre, final String saCui, final String saCarnet) {
        mAuth.createUserWithEmailAndPassword(saCorreo, saPass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            writeNewUser(saUsuario, saCorreo, saNombre, saCui, saCarnet);
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(Tag.TAG_AUTH_REGISTER, "createUserWithEmail:success");
                            safeUser(saUsuario);
                            // Toast.makeText(RegistrarUsuario.this, "usuario creado", Toast.LENGTH_SHORT).show();
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);

                        } else {
                            //Toast.makeText(RegistrarUsuario.this, "correo invalido", Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                    }
                });
    }




    private boolean validarEditTextnull() {

        String sUsuario = usuario.getText().toString();


        if (sUsuario.isEmpty()) {
            usuario.setError("usuario obligatorio");
        }


        return !(sUsuario.isEmpty());


    }


    private void writeNewUser(String sUsuario, String sCorreo, String sNombre, String sCui, String sCarnet) {


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        String key = database.getReference("usuarios").push().getKey();

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        long millis = new Date().getTime();

        Usuario user = new Usuario(sUsuario, sCorreo, sNombre, sCui, sCarnet, millis,false);

        mDatabase.child("usuarios").child(key).setValue(user);


    }

    public void fabExit(View view) {
        finish();
    }

    public void iniciarSession(View view) {

        ingresar(view);

    }

    private void safeUser(String s) {
        SharedPreferences prefs = getSharedPreferences(Constantes.EMAIL, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constantes.USER, s);
        editor.apply();
    }


}
