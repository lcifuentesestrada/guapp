package com.miterabit.guapp.vistas.auth;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.miterabit.guapp.MainActivity;
import com.miterabit.guapp.R;
import com.miterabit.guapp.controlador.RecyclerViewAdapterList;
import com.miterabit.guapp.dialogException.CuentaBaneadaDialog;
import com.miterabit.guapp.dialogException.SOSDIalog;
import com.miterabit.guapp.modelo.DenunciasSearch;
import com.miterabit.guapp.utils.Constantes;
import com.miterabit.guapp.utils.Tag;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class Timeline extends AppCompatActivity {


    private RecyclerView recyclerView;

    ArrayList<String> keyArraylist = new ArrayList<>();
    ArrayList<String> descripcionArray = new ArrayList<>();
    ArrayList<String> pruebasArray = new ArrayList<>();
    ArrayList<String> tipoArray = new ArrayList<>();
    ArrayList<String> tituloArray = new ArrayList<>();
    ArrayList<String> usuarioArray = new ArrayList<>();
    ArrayList<String> timestampArray = new ArrayList<>();
    ArrayList<Boolean> estadoArray = new ArrayList<>();

    FirebaseRemoteConfig mFirebaseRemoteConfig;
    private FirebaseAuth mAuth;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);

        TextView textView = (TextView) findViewById(R.id.toolbar_textview);
        textView.setText("Denuncias");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView.setLayoutManager(layoutManager);


        consultarFirebase();

        mAuth = FirebaseAuth.getInstance();


        verificar();


        Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isFirstRun", true);
        if (isFirstRun) {
            tutorialInterno();
        }
        getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putBoolean("isFirstRun", false).apply();


    }

    @SuppressLint("WrongConstant")
    private void tutorialInterno() {


        TapTargetView.showFor(this,

                TapTarget.forView(findViewById(R.id.imageView_sos), getResources().getString(R.string.titulo_sos), getResources().getString(R.string.cuerpo_sos))
                        .tintTarget(false)
                        .textColor(R.color.colorPrimaryDark)
                        .outerCircleColor(R.color.colorPrimary), new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);      // This call is optional
                        sostutorial();
                    }

                }
        );


    }

    private void sostutorial() {
        TapTargetView.showFor(this,
                TapTarget.forView(findViewById(R.id.fab_timeline), getResources().getString(R.string.titulo_denuncia), getResources().getString(R.string.cuerpo_denuncia))
                        .tintTarget(false)
                        .textColor(R.color.colorPrimaryDark)
                        .outerCircleColor(R.color.colorPrimary)
        );
    }

    private void verificar() {

        String e = mAuth.getCurrentUser().getEmail();

        SharedPreferences prefsUri = getSharedPreferences(Constantes.XT, Context.MODE_PRIVATE);
        String s = prefsUri.getString(Constantes.XT_P, null);

        try {
            mAuth.signInWithEmailAndPassword(e, s)
                    .addOnCompleteListener(this, task -> {

                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(Tag.TAG_MAIN, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                        } else {
                            Log.e(Tag.TAG_MAIN, "error:correo" + task.getException());


                            try {
                                if (task.getException().getMessage().contains("The user account has been disabled by an administrator.")) {

                                    final CuentaBaneadaDialog cuentaBaneadaDialog = new CuentaBaneadaDialog(Timeline.this);


                                    new Handler().postDelayed(() -> {
                                        finish();
                                        mAuth.signOut();
                                    }, 2500);


                                }

                                Log.d(Tag.TAG_MAIN, "error" + task.getException().getCause().getMessage());


                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }


                        }


                    });
        } catch (Exception ex) {
            Log.e(Tag.TAG_MAIN, "error: " + ex);
        }


    }


    public void nuevaDenuncia(View view) {
        startActivity(new Intent(this, ActivityAgregarNoticia.class));
    }

    public void cerrarSession(View view) {
        FirebaseAuth.getInstance().signOut();


        SharedPreferences prefs = getSharedPreferences(Constantes.AESAZAZEL, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Constantes.ADLV, false);
        editor.apply();


        startActivity(new Intent(this, MainActivity.class));
        this.finish();


    }


    public void sosAlert(View view) {


        new SOSDIalog(this);


    }


    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    private void consultarFirebase() {

        new Timeline.GetDataFromFirebase().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        // Read from the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("timeline");


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                keyArraylist.clear();
                descripcionArray.clear();
                pruebasArray.clear();
                tipoArray.clear();
                tituloArray.clear();
                usuarioArray.clear();
                timestampArray.clear();
                estadoArray.clear();

                for (DataSnapshot detalle : dataSnapshot.getChildren()) {
                    //  System.out.println(alert.getValue());
                    DenunciasSearch listado = detalle.getValue(DenunciasSearch.class);

                    try {
                        Log.i(Tag.KEY_DETALLE_FINAL, "|-------------------------DENUNCIA------------------------|--->");
                        Log.i(Tag.KEY_DETALLE_FINAL, "|                                                         |");
                        Log.i(Tag.KEY_DETALLE_FINAL, "|                                                         |");
                        Log.i(Tag.KEY_DETALLE_FINAL, "key_detalle: " + detalle.getKey());
                        Log.i(Tag.KEY_DETALLE_FINAL, "descripcion: " + listado.getDescripcion());
                        Log.i(Tag.KEY_DETALLE_FINAL, "pruebas: " + listado.getPruebas());
                        Log.i(Tag.KEY_DETALLE_FINAL, "tipo: " + listado.getTipo());
                        Log.i(Tag.KEY_DETALLE_FINAL, "fecha: " + listado.getTimestamp());
                        Log.i(Tag.KEY_DETALLE_FINAL, "titulo: " + listado.getTitulo());
                        Log.i(Tag.KEY_DETALLE_FINAL, "usuario: " + listado.getUsuario());
                        Log.i(Tag.KEY_DETALLE_FINAL, "estado: " + listado.isEstado());
                        Log.i(Tag.KEY_DETALLE_FINAL, "|                                                         |");
                        Log.i(Tag.KEY_DETALLE_FINAL, "|                                                         |");
                        Log.i(Tag.KEY_DETALLE_FINAL, "|---------------------------------------------------------|--->");


                        keyArraylist.add(detalle.getKey());
                        tituloArray.add(listado.getTitulo());
                        descripcionArray.add(listado.getDescripcion());
                        pruebasArray.add(listado.getPruebas());
                        tipoArray.add(listado.getTipo());
                        usuarioArray.add(listado.getUsuario());
                        timestampArray.add(getDate(listado.getTimestamp()));
                        estadoArray.add(listado.isEstado());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
                ArrayList androidVersions = prepareData();
                RecyclerViewAdapterList adapter = new RecyclerViewAdapterList(getApplicationContext(), androidVersions);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        return DateFormat.format("dd-MM-yyyy", cal).toString();
    }

    private ArrayList prepareData() {

        ArrayList android_version = new ArrayList<>();
        for (int i = 0; i < usuarioArray.size(); i++) {
            DenunciasSearch androidVersion = new DenunciasSearch();
            androidVersion.setDescripcion(descripcionArray.get(i));
            androidVersion.setPruebas(pruebasArray.get(i));
            androidVersion.setUid(keyArraylist.get(i));
            androidVersion.setTipo(tipoArray.get(i));
            androidVersion.setTitulo(tituloArray.get(i));
            androidVersion.setUsuario(usuarioArray.get(i));
            androidVersion.setFechacast(timestampArray.get(i));
            androidVersion.setEstado(estadoArray.get(i));
            android_version.add(androidVersion);

        }
        return android_version;
    }

    public void webview(View view) {
        startActivity(new Intent(this, ActivityWebView.class));
    }

    @SuppressLint("StaticFieldLeak")
    private class GetDataFromFirebase extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }
    }


}
