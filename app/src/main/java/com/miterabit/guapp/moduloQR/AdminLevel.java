package com.miterabit.guapp.moduloQR;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.miterabit.guapp.R;
import com.miterabit.guapp.utils.Constantes;
import com.miterabit.guapp.utils.Tag;
import com.miterabit.guapp.vistas.auth.Timeline;

import java.util.Objects;

public class AdminLevel extends AppCompatActivity {

    EditText correo, pass;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_level);
        mAuth = FirebaseAuth.getInstance();
        correo = findViewById(R.id.admin_editText_correo);
        pass = findViewById(R.id.admin_editText_password);


    }


    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }


    public void iniciarSe(View view) {

        if (correo.getText().toString().isEmpty()) {

            correo.setError("campo requerido");

        }

        if (pass.getText().toString().isEmpty()) {
            correo.setError("campo requerido");
        }

        if (!correo.getText().toString().isEmpty() && !pass.getText().toString().isEmpty()) {

            iniciar(correo.getText().toString(), pass.getText().toString(), view);


        }


    }


    private void safeEmail(String var1) {
        SharedPreferences prefs = getSharedPreferences(Constantes.EMAIL, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constantes.CORREO, var1);
        editor.apply();
        Log.d(Tag.PREFERENCIAS_GUARDADAS, "email" + var1);
    }


    private void safeBypass() {

        SharedPreferences prefs = getSharedPreferences(Constantes.AESAZAZEL, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Constantes.ADLV, true);
        editor.apply();

    }


    private void safeUser() {


        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        Query query = reference.child("usuarios").child("GU").orderByChild("nombre");


        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i(Constantes.LIKE, "" + dataSnapshot);
                if (dataSnapshot.exists()) {

                    String usuario = (String) dataSnapshot.child("usuario").getValue();


                    SharedPreferences prefs = getSharedPreferences(Constantes.EMAIL, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(Constantes.USER, usuario);
                    editor.apply();

                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(Tag.TAG_AUTH_REGISTER, "ERROR: " + databaseError);
            }


        });


    }


    private void iniciar(final String var1, String var2, final View view) {


        mAuth.signInWithEmailAndPassword(var1, var2)
                .addOnCompleteListener(this, task -> {

                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(Tag.TAG_MAIN, "signInWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        safeEmail(var1);
                        safeUser();
                        updateUI(user);
                        safeBypass();

                    } else {
                        Snackbar.make(view, "correo y/o contraseña erroneas", Snackbar.LENGTH_LONG)
                                .show();
                    }


                    Log.d(Tag.ADMIN_AUTH, "" + Objects.requireNonNull(task.getException()).getMessage());
                });


    }


    private void updateUI(FirebaseUser user) {
        if (user != null) {
            startActivity(new Intent(this, Timeline.class));
            finish();
        }
    }

}
