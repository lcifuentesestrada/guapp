package com.miterabit.guapp.moduloQR;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.Result;
import com.miterabit.guapp.R;
import com.miterabit.guapp.dialogException.CuentaBaneadaDialog;
import com.miterabit.guapp.dialogException.QRDialogException;
import com.miterabit.guapp.modelo.Usuario;
import com.miterabit.guapp.utils.Constantes;
import com.miterabit.guapp.utils.Tag;
import com.miterabit.guapp.vistas.auth.RegistrarUsuario;
import com.miterabit.guapp.vistas.auth.Timeline;
import com.scottyab.aescrypt.AESCrypt;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QrScanner extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView escannerView;
    String nombre, cui, carnet;
    private FirebaseAuth mAuth;
    ArrayList<String> busqueda = new ArrayList<>();
    String UserN;

    boolean v = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_scanner);
        mAuth = FirebaseAuth.getInstance();
        busqueda.clear();

    }


    private boolean verficarPermisos() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.CAMERA
            }, 225);
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void handleResult(final Result result) {

        if (admin(result)) {

            Log.i(Tag.TAG_AUTH_REGISTER, "<-CARNET ADMIN_LEVEL->");

            startActivity(new Intent(this, AdminLevel.class));
            finish();


        } else {

            if (comparar(result)) {


                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final StringBuilder builder = new StringBuilder();

                        try {
                            Document doc = Jsoup.connect(result.getText()).get();
                            Elements links = doc.select("a[href]");
                            Elements pTags = doc.select("p");
                            Elements hTags = doc.select("h3");


                            int i = 0;
                            for (Element link : pTags) {

                                builder.append("\n").append(link.attr("p")).append("").append("Info : ").append(link.text());

                                i++;

                                if (i == 1) {
                                    Log.i(Tag.LECTURA, "nombre: " + link.text());
                                    nombre = link.text();
                                }
                                if (i == 2) {
                                    Log.i(Tag.LECTURA, "cui: " + link.text());
                                    cui = (link.text());
                                }
                                if (i == 3) {
                                    Log.i(Tag.LECTURA, "carnet: " + link.text());
                                    carnet = (link.text());
                                }
                            }


                        } catch (IOException e) {
                            builder.append("Error : ").append(e.getMessage()).append("\n");
                        }

                        runOnUiThread(() -> {

                            Log.i(Tag.LECTURA, "PAGINA: " + builder.toString());


                            DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                            Query query = reference.child("usuarios").orderByChild("carnet");
                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        Log.i(Tag.TAG_AUTH_REGISTER, "->" + dataSnapshot);
                                        // dataSnapshot is the "issue" node with all children with id 0
                                        for (DataSnapshot issue : dataSnapshot.getChildren()) {

                                            Usuario usuario = issue.getValue(Usuario.class);

                                            if (usuario != null) {

                                                if (usuario.getCarnet().equals(carnet)) {

                                                    busqueda.add("si");
                                                    UserN = usuario.getUsuario();

                                                } else {
                                                    UserN = usuario.getUsuario();
                                                    busqueda.add("no");

                                                }


                                            }

                                        }


                                        if (verificarRegisrado()) {
                                            Log.i(Tag.TAG_AUTH_REGISTER, "<-CARNET REGISTRADO->");
                                            //Toast.makeText(QrScanner.this, "carnet ya registrado", Toast.LENGTH_SHORT).show();
                                            iniciar(carnet + Constantes.CORREOGU, cui + carnet + Constantes.AESAZAZEL);
                                            safe(cui + carnet + Constantes.AESAZAZEL);
                                            Log.i(Tag.TAG_AUTH_REGISTER, "<-===============" + carnet + "====================>");
                                            safeEmail(carnet + Constantes.CORREOGU);
                                            safeUser(UserN);
                                            Log.i(Tag.PREFERENCIAS_GUARDADAS, UserN);

                                            // Toast.makeText(QrScanner.this, "tiene", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Intent registrarDatos = new Intent(getApplicationContext(), RegistrarUsuario.class);
                                            registrarDatos.putExtra(Tag.EXTRA_NOMBRE, nombre);
                                            registrarDatos.putExtra(Tag.EXTRA_CUI, cui);
                                            registrarDatos.putExtra(Tag.EXTRA_CARNET, carnet);
                                            safeEmail(carnet + Constantes.CORREOGU);
                                            safe(cui + carnet + Constantes.AESAZAZEL);
                                            safeUser(UserN);
                                            Log.i(Tag.PREFERENCIAS_GUARDADAS, UserN);
                                            startActivity(registrarDatos);
                                            Log.i(Tag.TAG_AUTH_REGISTER, "<-CARNET NO REGISTRADO->");
                                            //Toast.makeText(QrScanner.this, "no", Toast.LENGTH_SHORT).show();
                                        }


                                    } /*else if (!dataSnapshot.exists()) {
                                        Intent registrarDatos = new Intent(getApplicationContext(), RegistrarUsuario.class);
                                        registrarDatos.putExtra(Tag.EXTRA_NOMBRE, nombre);
                                        registrarDatos.putExtra(Tag.EXTRA_CUI, cui);
                                        registrarDatos.putExtra(Tag.EXTRA_CARNET, carnet);
                                        safeEmail(carnet + Constantes.CORREOGU);
                                        safe(cui + carnet + Constantes.AESAZAZEL);
                                        startActivity(registrarDatos);
                                        Log.i(Tag.TAG_AUTH_REGISTER, "<-NO EXISTEN USUARIOS->");

                                    }*/
                                }


                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e(Tag.TAG_AUTH_REGISTER, "ERROR: " + databaseError);
                                }


                            });


                        });
                    }
                }).start();

            } else {

                new QRDialogException(this);
                escannerView.resumeCameraPreview(this);


            }

        }
    }

    private boolean verificarRegisrado() {

        for (String str : busqueda) {
            if (str.trim().equals("si"))
                return true;
        }
        return false;
    }

    private boolean admin(Result result) {

        String urlbase = Constantes.LUM;
        Log.d("especial", result + " = " + urlbase);
        return result.getText().contains(urlbase);
    }

    private void safeEmail(String s) {
        SharedPreferences prefs = getSharedPreferences(Constantes.EMAIL, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constantes.CORREO, s);
        editor.apply();
        Log.d(Tag.PREFERENCIAS_GUARDADAS, "email" + s);
    }

    private void safe(String s) {
        SharedPreferences prefs = getSharedPreferences(Constantes.XT, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constantes.XT_P, s);
        editor.apply();
    }


    private void safeUser(String s) {
        SharedPreferences prefs = getSharedPreferences(Constantes.EMAIL, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constantes.USER, s);
        editor.apply();
    }


    private void iniciar(String var1, String var2) {
        mAuth.signInWithEmailAndPassword(var1, var2)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(Tag.TAG_MAIN, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            Log.e(Tag.TAG_MAIN, "error:correo" + task.getException());


                            try {
                                if (task.getException().getMessage().contains("The user account has been disabled by an administrator.")) {

                                    final CuentaBaneadaDialog cuentaBaneadaDialog = new CuentaBaneadaDialog(QrScanner.this);


                                    new Handler().postDelayed(new Runnable() {
                                        public void run() {
                                            finish();
                                        }
                                    }, 2500);


                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }


                    }
                });

    }

    private void updateUI(FirebaseUser user) {
        Log.i(Tag.TAG_MAIN, "usuario logeado: " + user);
        if (user != null) {
            startActivity(new Intent(this, Timeline.class));
            this.finish();
        }
    }

    private boolean comparar(Result result) {
        String urlbase = "http://registro.usac.edu.gt/generaCarne/DatosEstudiante.php?rac=";
        return result.getText().contains(urlbase);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (escannerView != null) {
            escannerView.stopCamera();
        }
    }

    public void fabExit(View view) {
        finish();
    }


    public void iniciarScannerQR(View view) {

        if (verficarPermisos()) {

            escannerView = new ZXingScannerView(this);
            setContentView(escannerView);
            escannerView.setResultHandler(this);
            escannerView.startCamera();
        }


    }


}
